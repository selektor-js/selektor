import {TemplateConfiguration, ValidTemplateConfiguration} from './template-configuration';
import {RemoteConfiguration, ValidRemoteConfiguration} from './remote-configuration';
import {VirtualOptionConfiguration, ValidVirtualOptionConfiguration} from './virtual-option-configuration';
import {LoaderInterface} from '../virtual-option';
import {TemplateEngine} from '../templating';
import {OptionManager} from '../option/option-manager';

type virtualLoaderBuilder = (
    $select: HTMLSelectElement,
    templateEngine: TemplateEngine<any>,
    optionManager: OptionManager<any>,
    virtualOptionConfiguration: VirtualOptionConfiguration
) => LoaderInterface<any>;

export interface Configuration {
    multiple?: boolean;
    placeholder?: string;
    templates?: TemplateConfiguration;
    remote?: RemoteConfiguration|string;
    virtualLoader?: virtualLoaderBuilder;
    virtualOptions?: VirtualOptionConfiguration;
}

export interface ValidConfiguration {
    multiple: boolean;
    placeholder: string;
    templates: ValidTemplateConfiguration;
    remote: ValidRemoteConfiguration|null;
    virtualLoader?: virtualLoaderBuilder;
    virtualOptions: ValidVirtualOptionConfiguration;
}
