import {Option} from '../option';

type mainTemplate = (cssClasses: string|null) => string;
type optionTemplate = (option: Option<object>) => string;
type optgroupTemplate = (label: string, data?: object) => string;
type itemTemplate = (option: Option<object>) => string;
type nooptionTemplate = () => string;

export interface TemplateConfiguration {
    main?: mainTemplate;
    option?: optionTemplate|string;
    optgroup?: optgroupTemplate;
    item?: itemTemplate;
    nooption?: nooptionTemplate;
    cssClasses?: string;
}

export interface ValidTemplateConfiguration {
    main: mainTemplate;
    option: optionTemplate;
    optgroup: optgroupTemplate;
    item: itemTemplate;
    nooption: nooptionTemplate;
    cssClasses: string|null;
}
