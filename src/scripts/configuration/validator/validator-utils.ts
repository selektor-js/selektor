export abstract class ValidatorUtils {
    private constructor() {}

    public static throwTypeError(option: string, type: string, complement: string = ''): void {
        throw new Error(`The option "${option}" must be of type "${type}. ${complement}`);
    }

    public static throwEmptyError(value: undefined|null|Array<any>|string, option: string, complement: string = ''): void {
        if (
            (undefined === value) ||
            (null === value) ||
            ((Array.isArray(value)) && (0 === value.length)) ||
            (('string' === typeof value) && (0 === value.trim().length))
        ) {
            throw new Error(`The option "${option}" must not be empty. ${complement}`);
        }
    }
}
