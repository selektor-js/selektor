import {ValidVirtualOptionConfiguration, VirtualOptionConfiguration} from "../virtual-option-configuration";

export abstract class VirtualOptionValidator {
    public static validate(config: VirtualOptionConfiguration): ValidVirtualOptionConfiguration {
        const validConfig: ValidVirtualOptionConfiguration = Object.assign<object, VirtualOptionConfiguration>({
            moveTop: false,
        }, config) as ValidVirtualOptionConfiguration;

        return validConfig;
    }
}