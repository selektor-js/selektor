export interface VirtualOptionConfiguration {
    moveTop?: boolean;
}

export interface ValidVirtualOptionConfiguration {
    moveTop: boolean;
}
