import {TemplateEngine, TemplateUtils} from '../templating';
import {OptionManager} from '../option/option-manager';
import {Option, OptionManagerEvent} from '../option';

export class ItemCollection<T extends object> {
    private $items: Map<string, HTMLElement> = new Map();
    private inputInsideCollection: boolean;

    constructor(
        private templateEngine: TemplateEngine<T>,
        private optionManager: OptionManager<T>,
    ) {
        this.inputInsideCollection = TemplateUtils.contained(this.templateEngine.$itemCollection, this.templateEngine.$input);
        const selected = this.optionManager.selected;
        if (Array.isArray(selected)) {
            for (const option of selected) {
                this.addItem(option);
            }
        } else if (null !== selected) {
            this.addItem(selected);
        }

        this.optionManager.addListener('select', (e: OptionManagerEvent<T>) => {
            this.addItem(e.targetOption);
        });

        this.optionManager.addListener('unselect', (e: OptionManagerEvent<T>) => {
            this.removeItem(e.targetOption);
        });
    }

    get items(): Map<string, HTMLElement> {
        return this.$items;
    }

    private addItem(option: Option<T>): void {
        if (this.$items.has(option.value)) {
            return;
        }

        const $item = this.templateEngine.renderItem(option);
        this.$items.set(option.value, $item);

        $item.querySelector('[data-selektor-item-delete]')?.addEventListener('click', () => {
            option.selected = false;
        });

        if (this.inputInsideCollection) {
            TemplateUtils.insertBefore($item, this.templateEngine.$input);
        } else {
            this.templateEngine.$itemCollection.appendChild($item);
        }
    }

    private removeItem(option: Option<T>): void {
        const $item = this.$items.get(option.value);

        if (null !== $item) {
            this.$items.delete(option.value);
            $item.remove();
        }
    }
}
