import {Option} from './option';

export class OptionManagerEvent<T extends object> {
    constructor(
        readonly targetOption: Option<T>,
        readonly selected: Array<Option<T>>|Option<T>|null,
    ) {
    }
}
