import {EventEmitter} from '../event-emitter';

export class Option<T extends object> {
    readonly value: string;
    readonly label: string;
    readonly data: T|null;
    private isSelected: boolean;
    private isDisabled: boolean = false;
    private eventEmitter = new EventEmitter();

    constructor($opt: HTMLOptionElement);
    constructor(value: string, label: string, selected?: boolean, data?: T);
    constructor(value: HTMLOptionElement|string, label?: string, selected?: boolean, data?: T) {
        if (value instanceof HTMLOptionElement) {
            if (value.hasAttribute('data-selektor-option-data')) {
                data = JSON.parse(value.getAttribute('data-selektor-option-data'));
            }

            label = value.label;
            selected = value.selected;
            value = value.value;
        }

        if ('string' !== typeof value) {
            value = String(value).toString();
        }

        this.value = value.trim();

        if (undefined === label) {
            throw new Error('Option second argument (label) has to be set.');
        }

        this.label = label;
        this.isSelected = selected ?? false;
        this.data = data ?? null;
    }

    public get selected(): boolean {
        return this.isSelected;
    }

    public set selected(val: boolean) {
        if (val !== this.isSelected) {
            this.isSelected = val;
            this.eventEmitter.emit(val ? 'select' : 'unselect', this);
        }
    }

    public get disabled(): boolean {
        return this.isDisabled;
    }

    public set disabled(val: boolean) {
        if (val !== this.isDisabled) {
            this.isDisabled = val;
            this.eventEmitter.emit(val ? 'disable' : 'enable', this);

            if (val) {
                this.selected = false;
            }
        }
    }

    public toggleSelected(): boolean {
        return this.selected = !this.selected;
    }

    public addListener(event: 'select'|'unselect'|'disable'|'enable', callback: (option: Option<T>) => void): void {
        this.eventEmitter.addListener(event, callback);
    }
}
