import {ValidTemplateConfiguration} from '../configuration';
import {Utils} from './utils';
import {Option} from '../option';
import {EventEmitter} from '../event-emitter';
import {TemplateEvent} from './event/template-event';
import {OptionTemplateEvent} from './event/option-template-event';

export class Engine<T extends object> {
    readonly $render: HTMLElement;
    /** HTMLElement where Virtual Collection will by populate. */
    readonly $virtualCollection: HTMLElement;
    /** HTMLElement wrapping Virtual Collection. */
    readonly $virtualCollectionWrapper: HTMLElement;
    /** HTMLElement for Virtual Loading. */
    readonly $virtualLoading: HTMLElement|null;
    /** HTMLInputElement use to filter/search virtual options. */
    readonly $input: HTMLInputElement;
    /** HTMLElement containing selected elements to display wil dropdown closed. */
    readonly $itemCollection: HTMLElement;

    private loadingCounter: number = 0;
    private eventEmitter = new EventEmitter();

    constructor(
        private $select: HTMLSelectElement,
        private config: ValidTemplateConfiguration,
    ) {
        this.$select.hidden = true;
        this.$render = Utils.stringToHTML(this.config.main(this.config.cssClasses));
        this.eventEmitter.emit('main-template', new TemplateEvent(this.$render));
        Utils.insertAfter(this.$render, this.$select);

        this.$virtualCollection = this.querySelectorOrThrow('[data-selektor-virtual-collection]');
        this.$virtualCollectionWrapper = this.querySelectorOrThrow('[data-selektor-virtual-collection-wrapper]');
        this.$virtualLoading = this.$render.querySelector('[data-selektor-virtual-loading]');
        this.$input = this.querySelectorOrThrow<HTMLInputElement>('input[data-selektor-input]');
        this.$itemCollection = this.querySelectorOrThrow('[data-selektor-item-collection]');
    }

    public renderOption<U extends HTMLElement = HTMLElement>(option: Option<T>): U {
        const element: U = Utils.stringToHTML<U>(this.config.option(option));
        this.eventEmitter.emit('option-template', new OptionTemplateEvent(element, option));

        return element;
    }

    public renderOptGroup<U extends HTMLElement = HTMLElement>(label: string, data?: object): U {
        const element: U = Utils.stringToHTML<U>(this.config.optgroup(label, data));
        this.eventEmitter.emit('optgroup-template', new TemplateEvent(element));

        return element;
    }

    public renderItem<U extends HTMLElement = HTMLElement>(option: Option<T>): U {
        const element: U = Utils.stringToHTML<U>(this.config.item(option));
        this.eventEmitter.emit('item-template', new OptionTemplateEvent(element, option));

        return element;
    }

    public renderNoOption<U extends HTMLElement = HTMLElement>(): U {
        const element: U = Utils.stringToHTML<U>(this.config.nooption());
        this.eventEmitter.emit('nooption-template', new TemplateEvent(element));

        return element;
    }

    public get $labels(): Iterable<HTMLLabelElement> {
        if (0 === this.$select.id.trim().length) {
            return [];
        }

        return document.querySelectorAll<HTMLLabelElement>(`label[for="${this.$select.id}"]`)
    }

    public startLoading(): void {
        if (null !== this.$virtualLoading) {
            this.loadingCounter++;
            this.$virtualLoading.hidden = false;
        }
    }

    public endLoading(): void {
        if (null !== this.$virtualLoading) {
            this.loadingCounter--;
            this.loadingCounter = Math.max(0, this.loadingCounter);

            if (0 === this.loadingCounter) {
                this.$virtualLoading.hidden = true;
            }
        }
    }

    public addListener(event: 'main-template'|'optgroup-template'|'nooption-template', callback: (event: TemplateEvent) => void): void;
    public addListener(event: 'option-template'|'item-template', callback: (event: OptionTemplateEvent<T>) => void): void;
    public addListener(event: string, callback: (...args: any) => void): void {
        this.eventEmitter.addListener(event, callback);
    }

    private querySelectorOrThrow<U extends HTMLElement = HTMLElement>(selector: string): U {
        const $element = this.$render.querySelector<U>(selector);
        if (null === $element) {
            throw new Error(
                `Cannot find HTML element with "${selector}", you have to add it in your templates.`
            );
        }

        return $element;
    }
}
