import {LoaderInterface} from './loader';
import {VirtualOption} from '../virtual-option';
import {TemplateEngine} from '../../templating';
import {LoadData} from './load-data';
import {OptionManager} from '../../option/option-manager';
import {VirtualOptionConfiguration} from "../../configuration/virtual-option-configuration";

export class DomLoader<T extends object> implements LoaderInterface<T> {
    private initialized: boolean = false;
    private options: Array<VirtualOption<T>> = [];

    constructor(
        private $select: HTMLSelectElement,
        private templateEngine: TemplateEngine<T>,
        private optionManager: OptionManager<T>,
        private virtualOptionConfiguration: VirtualOptionConfiguration,
    ) {
    }

    public get loading(): boolean {
        return false;
    }

    public load(search: string = '', page: number = 1): Promise<LoadData<T>> {
        if (!this.initialized) {
            this.loadFromDOM();
        }

        return new Promise((resolve, reject) => {
            if (1 < page) {
                reject(new Error('Cannot get more then the first page with DOMLoader.'));
                return;
            }

            resolve({
                options: this.options,
                page: page,
                finished: true,
                search: search,
            });
        });
    }

    private loadFromDOM(): void {
        this.options = [];
        for (const $opt of this.$select.children) {
            if ($opt instanceof HTMLOptionElement) {
                if (0 === $opt.value.trim().length) {
                    continue;
                }

                const opt = this.optionManager.getOptionFor($opt);

                if (opt) {
                    this.options.push(new VirtualOption<T>(this.templateEngine.renderOption(opt), opt, this.virtualOptionConfiguration.moveTop));
                }
            } else if ($opt instanceof HTMLOptGroupElement) {
                const vOpt = new VirtualOption<T>(this.templateEngine.renderOptGroup($opt.label));
                let hasOpts = false;

                for (const $subOpt of $opt.children) {
                    if ($subOpt instanceof HTMLOptionElement) {
                        const opt = this.optionManager.getOptionFor($subOpt);

                        if (opt) {
                            hasOpts = true;
                            vOpt.addChild(new VirtualOption(this.templateEngine.renderOption(opt), opt, this.virtualOptionConfiguration.moveTop));
                        }
                    }
                }

                if (hasOpts) {
                    this.options.push(vOpt);
                }
            }
        }
    }
}
