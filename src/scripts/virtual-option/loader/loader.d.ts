import {LoadData} from './load-data';

export interface LoaderInterface<T extends object> {
    readonly loading: boolean;
    load(search?: string, page?: number): Promise<LoadData<T>>;
}
