import {LoaderInterface} from './loader';
import {LoadData} from './load-data';
import {TemplateEngine} from '../../templating';
import {ValidRemoteConfiguration} from '../../configuration/remote-configuration';
import {VirtualOption} from '../virtual-option';
import {OptionManager} from '../../option/option-manager';
import {VirtualOptionConfiguration} from "../../configuration/virtual-option-configuration";

export class RemoteLoader<T extends object> implements LoaderInterface<T> {
    private isLoading: boolean = false;
    private abordAllow: boolean = false;
    private controller: AbortController|null = null;

    constructor(
        private templateEngine: TemplateEngine<T>,
        private optionManager: OptionManager<T>,
        private configuration: ValidRemoteConfiguration,
        private virtualOptionConfiguration: VirtualOptionConfiguration,
    ) {
        this.abordAllow = 'function' === typeof AbortController;
    }

    public get loading(): boolean {
        return this.isLoading;
    }

    public load(search: string = '', page: number = 1): Promise<LoadData<T>> {
        if (0 > page) {
            return Promise.reject(new Error('Cannot get less then the first page with RemoteLoader.'));
        }

        this.isLoading = true;

        if (null !== this.controller) {
            this.controller.abort();
            this.controller = null;
        }

        const opts: RequestInit = {};
        if (this.abordAllow) {
            this.controller = new AbortController();
            opts.signal = this.controller.signal;
        }

        let url = this.configuration.url;
        url += url.match(/\?/) ? '&' : '?';
        url += this.configuration.queryBuilder(search, page);

        return fetch(url, opts)
            .then((response: Response) => {
                return response.json().then((data) => {
                    return {
                        data: data,
                        response: data,
                    } as {data: any, response: Response};
                });
            })
            .then((info) => {
                const options = this.configuration.dataOptionConverter(info.data);
                return {
                    options: options.map((remoteOption: {value: string, label: string, data?: T}) => {
                        const opt = this.optionManager.getOptionFor(
                            remoteOption.value,
                            remoteOption.label,
                            false,
                            remoteOption.data,
                        );

                        return new VirtualOption(this.templateEngine.renderOption(opt), opt, this.virtualOptionConfiguration.moveTop);
                    }),
                    page: page,
                    finished: this.configuration.checkFinished(options, info.data, info.response),
                    search: search,
                };
            })
            .finally(() => {
                this.isLoading = false;
            })
            .catch((e) => {
                this.isLoading = false;
                return e;
            });
    }
}
