import {TemplateUtils} from '../templating';

export class TextNode {
    readonly $startComment: Comment;
    readonly $endComment: Comment;
    readonly searchableText: string;

    constructor(
        private $text: Text,
    ) {
        this.$text.data = TextNode.canonicalizeString(this.$text.data);
        this.searchableText = this.$text.data.toLowerCase();

        if (!this.empty) {
            this.$endComment = document.createComment('[selektor-text-filterable]');
            TemplateUtils.insertAfter(this.$endComment, $text);

            this.$startComment = document.createComment('[selektor-text-filterable]');
            TemplateUtils.insertBefore(this.$startComment, $text);
        }
    }

    public get empty(): boolean {
        return '' === this.$text.data;
    }

    public contain(search: string): boolean {
        return -1 !== this.searchableText.search(TextNode.canonicalizeString(search));
    }

    public mark(search: string): void {
        search = TextNode.canonicalizeString(search);
        this.reset();
        TemplateUtils.detach(this.$text);
        const index = this.searchableText.search(search);

        if (-1 !== index) {
            let template = '';
            if (0 < index) {
                template += this.$text.data.slice(0, index);
            }

            template += `<mark>${this.$text.data.slice(index, index + search.length)}</mark>`;
            template += this.$text.data.slice(index + search.length, this.searchableText.length);

            TemplateUtils.insertAfter(TemplateUtils.stringToHTMLs(template), this.$startComment);
        }
    }

    public reset(): void {
        const $parent = this.$startComment.parentNode;
        if (null === $parent) {
            return;
        }

        let $current: Node = this.$startComment.nextSibling;
        while ($current) {
            const $next = $current.nextSibling;
            if ($current === this.$endComment) {
                break;
            }

            TemplateUtils.detach($current);
            $current = $next;
        }

        TemplateUtils.insertAfter(this.$text, this.$startComment);
    }

    private static canonicalizeString(str: string): string {
        return str.trim().replace( /(\s)\s+/g, '$1')
    }
}
